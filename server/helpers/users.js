import casual from 'casual';

casual.define('user', () => ({
  id: casual.uuid,
  firstName: casual.first_name,
  lastName: casual.last_name,
  address: {
    houseNumber: casual.address,
    street: casual.street,
    postcode: casual.zip(7),
    country: casual.country
  },
  contactDetails: {
    email: casual.email,
    phone: casual.phone
  }
}));

const generateUsers = count =>
  Array.from(Array(count)).map(() => casual._user());

const NUM_USERS = 2000;

export const users = generateUsers(NUM_USERS);
