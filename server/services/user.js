import { users } from '../helpers/users';

export class UserService {
  getUsers(name) {
    return name
      ? users.filter(({ firstName, lastName }) =>
          `${firstName} ${lastName}`
            .toLowerCase()
            .startsWith(name.toLowerCase())
        )
      : users;
  }
}
