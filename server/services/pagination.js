const DEFAULT_KEY = 'id';

const DEFAULT_PAGE_SIZE = 10;

const getPageSize = limit => {
  if (!limit || limit <= 0 || limit > 100) {
    return DEFAULT_PAGE_SIZE;
  }
  return limit;
};

const getStart = (list, key, { after, before, last }) => {
  const predicate = cursor => item => item[key] === cursor;

  if (after) {
    const index = list.findIndex(predicate(after));
    if (index > -1) {
      return index + 1;
    }
  }
  if (before) {
    const index = list.findIndex(predicate(before));
    if (index > last) {
      return index - last;
    }
  }
  return 0;
};

export class PaginationService {
  paginate(
    { list = [], key = DEFAULT_KEY, gqlType = 'Node' },
    { first, after, last, before }
  ) {
    if (after && before) {
      throw new Error(
        'invalid.cursor.param',
        'Cannot accept forward and backward cursor parameters together'
      );
    }

    const pageSize = getPageSize(after ? first : last || first);
    const totalCount = list.length;
    const start = getStart(list, key, { after, before, last });
    const page = pageSize ? list.slice(start, start + pageSize) : list;
    const firstItem = page.length ? page[0] : null;
    const lastItem = page.length ? page[page.length - 1] : null;
    const pageNumber = Math.floor(start / pageSize);

    const startCursor = firstItem && firstItem[key];
    const endCursor = lastItem && lastItem[key];
    const edges = page.map(item => ({
      cursor: item[key],
      node: { ...item, gqlType }
    }));
    const hasNextPage = start + pageSize < totalCount;
    const hasPreviousPage = start > 0;

    const pageInfo = {
      startCursor,
      endCursor,
      hasNextPage,
      hasPreviousPage,
      pageNumber,
      totalCount
    };
    return {
      edges,
      pageInfo
    };
  }
}
