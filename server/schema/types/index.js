import Address from './address.graphql';
import ContactDetails from './contact-details.graphql';
import Edge from './edge.graphql';
import Node from './node.graphql';
import PageInfo from './page-info.graphql';
import Page from './page.graphql';
import User from './user.graphql';

export default () => [
  Address,
  ContactDetails,
  User,
  Edge,
  Node,
  Page,
  PageInfo
];
