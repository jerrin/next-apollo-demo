import RootQuery from './root-query.graphql';
import Types from './types';

const SchemaDefinition = `
schema {
  query: RootQueryType
}`;

export const typeDefs = [SchemaDefinition, Types, RootQuery];
