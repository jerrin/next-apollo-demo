import { PaginationService, UserService } from '../../services';

const paginationService = new PaginationService();
const userService = new UserService();

export default {
  users: (_, args) => {
    const { name, first, after, last, before } = args;
    const users = userService.getUsers(name);
    return paginationService.paginate(
      { list: users, gqlType: 'User' },
      { first, after, last, before }
    );
  }
};
