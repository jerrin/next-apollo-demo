import Name from './name';
import PageNode from './page-node';
import User from './user';

export const queries = {
  ...PageNode
};

export const rootQueries = {
  RootQueryType: {
    ...Name,
    ...User
  }
};
