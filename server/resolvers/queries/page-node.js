export default {
  PageNode: {
    __resolveType: obj => {
      return obj?.gqlType || 'Node';
    }
  }
};
