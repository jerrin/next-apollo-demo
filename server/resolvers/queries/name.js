import faker from 'faker';

export default {
  name: () => {
    return faker.name.findName();
  }
};
