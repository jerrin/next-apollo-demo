import { queries, rootQueries } from './queries';

export default {
  ...queries,
  ...rootQueries
};
