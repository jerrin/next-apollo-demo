import { ApolloServer } from 'apollo-server-express';
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';

import myGraphQLSchema from './schema-definition';

const PATH_GRAPHQL = '/graphql';
const PATH_PLAYGROUND = '/graphiql';

const startApolloServer = async () => {
  const apollo = new ApolloServer({
    ...myGraphQLSchema,
    playground: false
  });

  const playground = new ApolloServer({
    ...myGraphQLSchema,
    playground: {
      endpoint: PATH_PLAYGROUND
    }
  });

  await apollo.start();
  await playground.start();

  const app = express();
  app.use(cors());

  app.use(PATH_GRAPHQL, bodyParser.json());
  apollo.applyMiddleware({ app, path: PATH_GRAPHQL });

  app.use(PATH_PLAYGROUND, bodyParser.json());
  playground.applyMiddleware({ app, path: PATH_PLAYGROUND });

  const port = process.env.PORT || 5000;
  await new Promise(resolve => app.listen({ port }, resolve));
  console.log(`Graphql Server started on: http://localhost:${port}`);
};

startApolloServer().catch(e =>
  console.log('Unable to start Graphql Server', e)
);
