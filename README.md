## Local setup

### Pre-requisites

**Yarn and Lerna**

```
npm install --global yarn lerna
```

### Installation

Run the following command to install the dependencies

```
lerna bootstrap
```

### Build & Run

Run the following commands to build and start graphql server

```
yarn build:server && yarn start:server
```

Run the following commands to build and start nexjs application

```
yarn build:client && yarn start:client
```

## Docker containers

Run the following commands to run dockerized graphql server

```
docker-compose -f docker-compose-server.yml build --no-cache
docker-compose -f docker-compose-server.yml up
```

Run the following commands to run dockerized nexjs application

```
docker-compose -f docker-compose-client.yml build --no-cache
docker-compose -f docker-compose-client.yml up
```

## Urls

Grapqhl playground: http://localhost:5000/graphiql

Nextjs app: http://localhost:3000/

## Tests

### Unit tests

```
yarn test:unit
```

### Integration tests

```
yarn start:server
yarn start:client
yarn test:integration:client
```



