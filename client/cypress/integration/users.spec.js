describe('Users page', () => {
  const assertUserCountToBe = count => {
    cy.get('[data-test-id="list"]')
      .find('[data-test-id="user-details"]')
      .should('have.length', count);
  };

  it('should navigate to the users listing page', () => {
    cy.visit('http://localhost:3000/');
    cy.get('a[href*="users"]').click();
    cy.url().should('include', '/users');
    cy.get('[data-test-id="heading"]').contains('Users');
  });

  it('should list 20 users on page load', () => {
    cy.visit('http://localhost:3000/users');
    cy.get('[data-test-id="heading"]').contains('Users');
    assertUserCountToBe(20);
  });

  it('should load 20 more on every scroll', () => {
    cy.visit('http://localhost:3000/users');
    cy.get('[data-test-id="heading"]').contains('Users');
    assertUserCountToBe(20);
    cy.window().scrollTo('bottom');
    assertUserCountToBe(40);
    cy.window().scrollTo('bottom');
    assertUserCountToBe(60);
  });

  it('should search user by name', () => {
    cy.visit('http://localhost:3000/users');
    cy.get('[data-test-id="heading"]').contains('Users');
    assertUserCountToBe(20);
    cy.get('[data-test-id="list"]')
      .find('[data-test-id="user-details"]')
      .then(list => {
        const index = Math.floor(Math.random() * 20);
        cy.wrap(list[index])
          .find('[data-test-id="name"]')
          .invoke('text')
          .as('name');
      });
    cy.get('@name').then(name => {
      cy.get('[data-test-id="search"]').type(name, { force: true });
      cy.get('[data-test-id="list"]')
        .find('[data-test-id="user-details"]')
        .then(list => {
          cy.wrap(list[0])
            .find('[data-test-id="name"]')
            .should('contain', name);
        });
    });
  });
});
