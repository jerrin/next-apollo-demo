import { gql, useQuery } from '@apollo/client';

const GET_NAME = gql`
  query getName {
    name
  }
`;

export const useName = () => {
  return useQuery(GET_NAME);
};
