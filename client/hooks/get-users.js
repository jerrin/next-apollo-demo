import { useQuery } from '@apollo/client';

import { GET_USERS } from '../graphql/get-users';

export const useUsers = pageSize => {
  return useQuery(GET_USERS, {
    variables: {
      first: pageSize
    }
  });
};
