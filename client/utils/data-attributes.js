export const getDataAttributes = id => ({
  'data-test-id': id
});
