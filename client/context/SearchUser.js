import { createContext, useContext, useEffect, useState } from 'react';

import { useUsers } from '../hooks/get-users';

const SearchContext = createContext();

const PAGE_SIZE = 20;

export const UserSearchProvider = ({ children }) => {
  const { data, loading, fetchMore } = useUsers(PAGE_SIZE);
  const [initialized, setInitialized] = useState(false);
  const [name, setName] = useState();

  useEffect(() => {
    setInitialized(true);
  }, []);

  useEffect(() => {
    if (initialized) {
      fetchMore({
        variables: {
          name
        }
      });
    }
  }, [name]);

  const { users } = data || {};
  const loadMore = paginationParams =>
    fetchMore({
      variables: {
        name,
        first: PAGE_SIZE,
        ...paginationParams
      }
    });

  return (
    <SearchContext.Provider value={{ setName, loading, users, loadMore }}>
      {children}
    </SearchContext.Provider>
  );
};

export const useUserSearch = () => {
  const { setName, users, loading, loadMore } = useContext(SearchContext);
  return { setName, users, loading, loadMore };
};
