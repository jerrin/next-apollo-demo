import { relayStylePagination } from '@apollo/client/utilities';

export const cacheConfig = {
  typePolicies: {
    Query: {
      fields: {
        users: relayStylePagination()
      }
    },
    User: {
      keyFields: ['id']
    }
  }
};
