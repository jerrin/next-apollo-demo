import { ApolloClient, InMemoryCache } from '@apollo/client';
import { withApollo } from 'next-apollo';
import getConfig from 'next/config';

import { cacheConfig } from './cache-config';

const { publicRuntimeConfig } = getConfig();

const apolloClient = new ApolloClient({
  uri: publicRuntimeConfig.graphqlEndpoint,
  cache: new InMemoryCache(cacheConfig)
});

export default withApollo(apolloClient);
