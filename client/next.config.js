module.exports = {
  publicRuntimeConfig: {
    graphqlEndpoint:
      process.env.GRAPHQL_ENDPOINT || 'http://localhost:5000/graphql'
  }
};
