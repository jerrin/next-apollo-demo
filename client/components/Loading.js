import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';

const variants = ['h1', 'h3', 'body1', 'caption'];

const Skeletal = () => {
  return (
    <div>
      {variants.map(variant => (
        <Typography component="div" key={variant} variant={variant}>
          <Skeleton animation="wave" />
        </Typography>
      ))}
    </div>
  );
};
const Loading = () => {
  return (
    <Grid container spacing={8}>
      <Grid item xs>
        <Skeletal />
      </Grid>
    </Grid>
  );
};

export default Loading;
