import { memo } from 'react';

import { Box } from '@material-ui/core';

import { getDataAttributes } from '../utils/data-attributes';
import Address from './Address';
import Contact from './Contact';

const User = ({ user }) => {
  const { firstName, lastName, address, contactDetails } = user;

  return (
    <Box my={1} p={1} border={1} {...getDataAttributes('user-details')}>
      <Box {...getDataAttributes('name')}>
        {firstName} {lastName}
      </Box>
      <Box my={1}>
        <Address address={address} />
      </Box>
      <Box my={1}>
        <Contact contact={contactDetails} />
      </Box>
    </Box>
  );
};

export default memo(User);
