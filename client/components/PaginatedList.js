import { useEffect } from 'react';

import { Box, CircularProgress, Grid } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';

import { getDataAttributes } from '../utils/data-attributes';
import Loading from './Loading';

const Loader = () => {
  return (
    <Box my={3} textAlign={'center'}>
      <CircularProgress />
    </Box>
  );
};

const PaginatedList = ({ data, ListItem, fetchMore }) => {
  const { pageInfo, edges } = data;
  const list = edges?.map(({ node }) => node);

  useEffect(() => {
    const el = document.getElementsByClassName('infinite-scroll-component ');
    if (el.length) {
      el[0].style.overflow = 'hidden';
    }
  }, []);

  const getMoreData = () => {
    if (!pageInfo.hasNextPage) {
      return;
    }
    fetchMore &&
      fetchMore({
        after: pageInfo.endCursor
      });
  };

  return (
    <InfiniteScroll
      dataLength={list.length}
      next={getMoreData}
      hasMore={pageInfo.hasNextPage}
      loader={<Loader />}
    >
      <Grid container spacing={3} {...getDataAttributes('list')}>
        {list &&
          list.map((item, index) => (
            <Grid item key={index} xs={12} sm={6} md={3}>
              <ListItem item={item} />
            </Grid>
          ))}
      </Grid>
    </InfiniteScroll>
  );
};

export default PaginatedList;
