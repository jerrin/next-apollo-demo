import { useEffect, useState } from 'react';

import { Box, CircularProgress, Grid } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';

import { getDataAttributes } from '../utils/data-attributes';

const PAGE_SIZE = 20;

const Loader = () => {
  return (
    <Box my={3} textAlign={'center'}>
      <CircularProgress />
    </Box>
  );
};

const List = ({ data, ListItem }) => {
  const [count, setCount] = useState({
    prev: 0,
    next: PAGE_SIZE
  });
  const [hasMore, setHasMore] = useState(true);
  const [current, setCurrent] = useState([]);

  useEffect(() => {
    const el = document.getElementsByClassName('infinite-scroll-component ');
    if (el.length) {
      el[0].style.overflow = 'hidden';
    }
  }, []);

  useEffect(() => {
    setCount({
      prev: 0,
      next: PAGE_SIZE
    });
    setCurrent(data.slice(0, PAGE_SIZE));
    setHasMore(data.length > PAGE_SIZE);
  }, [data]);

  const getMoreData = () => {
    if (current.length === data.length) {
      setHasMore(false);
      return;
    }
    setTimeout(() => {
      setCurrent(
        current.concat(
          data.slice(count.prev + PAGE_SIZE, count.next + PAGE_SIZE)
        )
      );
    }, 2000);
    setCount(prevState => ({
      prev: prevState.prev + PAGE_SIZE,
      next: prevState.next + PAGE_SIZE
    }));
  };

  return (
    <InfiniteScroll
      dataLength={current.length}
      next={getMoreData}
      hasMore={hasMore}
      loader={<Loader />}
    >
      <Grid container spacing={3} {...getDataAttributes('list')}>
        {current &&
          current.map((item, index) => (
            <Grid item key={index} xs={12} sm={6} md={3}>
              <ListItem item={item} />
            </Grid>
          ))}
      </Grid>
    </InfiniteScroll>
  );
};

export default List;
