import { useName } from '../hooks';

const Name = () => {
  const { data, loading } = useName();
  return <span>{loading ? '..' : data?.name}</span>;
};

export default Name;
