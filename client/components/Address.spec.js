/**
 * @jest-environment jsdom
 */

import React from 'react';

import { render, cleanup } from '@testing-library/react';

import { getDataAttributes } from '../utils/data-attributes';
import Address from './Address';

describe('Address', () => {
  const addressData = {
    houseNumber: 'houseNumber',
    street: 'street',
    postcode: 'postcode',
    country: 'country'
  };

  const renderComponent = () =>
    render(
      <Address
        address={addressData}
        dataAttributes={getDataAttributes('address')}
      />
    );

  afterEach(() => {
    cleanup();
  });

  it('should render the address component', () => {
    const { getByTestId } = renderComponent();
    expect(getByTestId('address')).toBeTruthy();
  });

  describe('Address fields', () => {
    it('should render house number', () => {
      const { getByTestId } = renderComponent();
      const addressElement = getByTestId('address');
      expect(
        addressElement.querySelector('[data-test-id="houseNumber"]')
      ).toHaveTextContent(addressData.houseNumber);
    });

    it('should render street', () => {
      const { getByTestId } = renderComponent();
      const addressElement = getByTestId('address');
      expect(
        addressElement.querySelector('[data-test-id="street"]')
      ).toHaveTextContent(addressData.street);
    });

    it('should render postcode', () => {
      const { getByTestId } = renderComponent();
      const addressElement = getByTestId('address');
      expect(
        addressElement.querySelector('[data-test-id="postcode"]')
      ).toHaveTextContent(addressData.postcode);
    });

    it('should render country', () => {
      const { getByTestId } = renderComponent();
      const addressElement = getByTestId('address');
      expect(
        addressElement.querySelector('[data-test-id="country"]')
      ).toHaveTextContent(addressData.country);
    });
  });
});
