import { Box } from '@material-ui/core';

import { getDataAttributes } from '../utils/data-attributes';

const Address = ({ address, dataAttributes }) => {
  const { houseNumber, street, postcode, country } = address;

  return (
    <Box {...dataAttributes}>
      <Box my={1}>Address:</Box>
      <Box {...getDataAttributes('houseNumber')}>{houseNumber}</Box>
      <Box {...getDataAttributes('street')}>{street}</Box>
      <Box {...getDataAttributes('postcode')}>{postcode}</Box>
      <Box {...getDataAttributes('country')}>{country}</Box>
    </Box>
  );
};

export default Address;
