import { Box } from '@material-ui/core';

const Contact = ({ contact }) => {
  const { email, phone } = contact;

  return (
    <Box>
      <Box>Email: {email}</Box>
      <Box>Phone: {phone}</Box>
    </Box>
  );
};

export default Contact;
