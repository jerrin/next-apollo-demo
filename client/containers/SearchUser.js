import { useEffect, useRef } from 'react';

import { TextField, debounce } from '@material-ui/core';

import { useUserSearch } from '../context';
import { getDataAttributes } from '../utils/data-attributes';

const SearchUser = () => {
  const inputRef = useRef(null);
  const { loading, setName } = useUserSearch();
  const debouncedSetName = debounce(setName, 500);

  useEffect(() => {
    if (!loading && inputRef.current) {
      inputRef.current.focus();
    }
  }, [loading, inputRef]);

  const onChange = e => {
    const val = e.target.value;
    debouncedSetName(val);
  };

  return (
    <TextField
      variant={'outlined'}
      size={'small'}
      label={'Search'}
      disabled={loading}
      onChange={onChange}
      inputProps={{ ...getDataAttributes('search') }}
      inputRef={inputRef}
    />
  );
};

export default SearchUser;
