import Loading from '../components/Loading';
import PaginatedList from '../components/PaginatedList';
import User from '../components/User';
import { useUserSearch } from '../context';

const UserItem = ({ item }) => <User user={item} />;

const ListUsers = () => {
  const { loading, users, loadMore } = useUserSearch();

  if (loading) return <Loading />;

  return (
    <PaginatedList data={users} ListItem={UserItem} fetchMore={loadMore} />
  );
};

export default ListUsers;
