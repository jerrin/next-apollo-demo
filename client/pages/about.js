import Link from 'next/link';

import { getDataAttributes } from '../utils/data-attributes';

const About = () => (
  <div>
    <div {...getDataAttributes('heading')}>About Page</div>
    <br />
    <br />
    <Link href="/">
      <a>Go Back</a>
    </Link>
  </div>
);

export default About;
