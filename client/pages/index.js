import { Grid } from '@material-ui/core';
import Link from 'next/link';

import WithApollo from '../apollo/client';
import Name from '../components/Name';

const Page = WithApollo({ ssr: true })(() => (
  <div>
    Welcome, <Name />
    <br />
    <br />
    <Grid container spacing={3}>
      <Grid item xs={12} sm={6} md={3}>
        <Link href="/about">
          <a>About</a>
        </Link>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <Link href="/users">
          <a>Users</a>
        </Link>
      </Grid>
    </Grid>
  </div>
));

export default Page;
