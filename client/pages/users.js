import {
  Container,
  AppBar,
  Toolbar,
  Typography,
  Box,
  Grid
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import WithApollo from '../apollo/client';
import ListUsers from '../containers/ListUsers';
import SearchUser from '../containers/SearchUser';
import { UserSearchProvider } from '../context';
import { getDataAttributes } from '../utils/data-attributes';

const useStyles = makeStyles(theme => ({
  toolbar: {
    justifyContent: 'center'
  },
  container: {
    marginTop: theme.spacing(12)
  }
}));

const Users = WithApollo({ ssr: true })(() => {
  const classes = useStyles();
  return (
    <UserSearchProvider>
      <AppBar color={'default'}>
        <Box display={'flex'} justifyContent={'center'}>
          <Typography variant={'h5'} {...getDataAttributes('heading')}>
            Users
          </Typography>
        </Box>
        <Toolbar className={classes.toolbar}>
          <Grid container justifyContent={'center'} spacing={1}>
            <SearchUser />
          </Grid>
        </Toolbar>
      </AppBar>
      <Container className={classes.container}>
        <ListUsers />
      </Container>
    </UserSearchProvider>
  );
});

export default Users;
