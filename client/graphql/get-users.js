import { gql } from '@apollo/client';

import { USER_FIELDS, PAGE_INFO_FIELDS } from './fragments';

export const GET_USERS = gql`
  ${USER_FIELDS}
  ${PAGE_INFO_FIELDS}
  query users(
    $name: String
    $first: Int
    $after: String
    $last: Int
    $before: String
  ) {
    users(
      name: $name
      first: $first
      after: $after
      last: $last
      before: $before
    ) {
      pageInfo {
        ...PageInfoFields
      }
      edges {
        cursor
        node {
          ... on User {
            ...UserFields
          }
        }
      }
    }
  }
`;
