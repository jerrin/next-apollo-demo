import { gql } from '@apollo/client';

export const ADDRESS_DETAILS = gql`
  fragment AddressFields on Address {
    houseNumber
    street
    postcode
    country
  }
`;

export const CONTACT_DETAILS = gql`
  fragment ContactDetailFields on ContactDetails {
    email
    phone
  }
`;

export const PAGE_INFO_FIELDS = gql`
  fragment PageInfoFields on PageInfo {
    startCursor
    endCursor
    hasNextPage
    hasPreviousPage
    pageNumber
    totalCount
  }
`;

export const USER_FIELDS = gql`
  ${ADDRESS_DETAILS}
  ${CONTACT_DETAILS}
  fragment UserFields on User {
    id
    firstName
    lastName
    address {
      ...AddressFields
    }
    contactDetails {
      ...ContactDetailFields
    }
  }
`;
